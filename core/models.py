from django.db import models

# Create your models here.
class FreelanceInput(models.Model):
	firstname = models.CharField(max_length=100)
	lastname = models.CharField(max_length=100)
	jobTitle = models.CharField(max_length=200)
	status = models.CharField(max_length=100)
	retribution = models.IntegerField(blank=True)
	availabilityDate = models.CharField(max_length=200)


class Skill(models.Model):
	name = models.CharField(max_length=200)


class ProfessionalExperiences(models.Model):
	companyName = models.CharField(max_length=200)
	startDate = models.DateTimeField()
	endDate = models.DateTimeField()
	skills